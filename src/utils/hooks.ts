"use client";

import { useLayoutEffect, useState } from "react";
import { Size2D } from "./types";

const useElementSize = () => {
  const [size, setSize] = useState<Size2D>({ width: 0, height: 0 });
  const [el, setEl] = useState<HTMLElement | null>(null);

  useLayoutEffect(() => {
    if (!el) return;

    const observer = new window.ResizeObserver((entries) => {
      const entry = entries[0];
      setSize({
        width: entry.contentRect.width,
        height: entry.contentRect.height,
      });
    });

    observer.observe(el);

    return () => observer.disconnect();
  }, [el]);

  return { setEl, width: size.width, height: size.height };
};

export { useElementSize };
