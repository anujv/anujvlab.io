export function cx(
  ...classNames: (string | { [key: string]: boolean } | null | undefined)[]
): string {
  let cls: string = "";

  for (let i = 0; i < classNames.length; i++) {
    const c = classNames[i];
    if (c === null || c === undefined) continue;

    if (typeof c === "string") {
      cls += ` ${c}`;
      continue;
    }

    const keys = Object.keys(c);
    keys.forEach((k) => {
      if (c[k]) cls += ` ${k}`;
    });
  }

  return cls;
}
