import React from "react";
import Link from "next/link";
import { cx } from "../../utils/helper";

type Props = {
  title: string;
  link?: string;
  className?: string;
};

const NavbarLink: React.FC<Props> = (props) => {
  const { title, link, className } = props;

  const elem = (
    <p className={cx("text-base text-primary font-medium", className)}>
      {title}
    </p>
  );

  return <li>{link ? <Link href={link}>{elem}</Link> : elem}</li>;
};

export default NavbarLink;
