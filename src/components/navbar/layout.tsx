import React from "react";
import { cx } from "../../utils/helper";

type Props = {
  className?: string;
  children?: React.ReactNode;
};

const NavbarLayout: React.FC<Props> = (props) => {
  const { className, children } = props;

  return (
    <div
      className={cx("flex items-center rounded-lg bg-nav px-4 py-3", className)}
    >
      {children}
    </div>
  );
};

export default NavbarLayout;
