import React from "react";
import { FaLinkedinIn } from "react-icons/fa";
import { FaGitlab, FaGithub } from "react-icons/fa6";
import { IconType } from "react-icons";

import NavbarLayout from "./layout";
import NavbarLink from "./link";
import { cx } from "../../utils/helper";
import { IconButton } from "../buttons";
import ThemeSwitcher from "./theme-switcher";

type Props = {
  className?: string;
};

type SocialLinkProps = {
  icon: IconType;
  link: string;
};

const SocialLink: React.FC<SocialLinkProps> = (props) => {
  const { icon, link } = props;

  return (
    <a href={link} target="_blank" rel="noopener noreferrer">
      <IconButton as="div" icon={icon} />
    </a>
  );
};

const Navbar: React.FC<Props> = (props) => {
  const { className } = props;

  return (
    <NavbarLayout className={cx("w-full justify-between", className)}>
      <div className="flex items-center gap-12">
        <p className="text-lg gradient-rose-1 bg-clip-text text-transparent font-semibold">
          Anuj
        </p>
        <ul className="hidden md:flex items-center gap-2">
          <NavbarLink title="work" />
          <NavbarLink title="projects" />
          <NavbarLink title="about" />
          <NavbarLink title="blog" />
        </ul>
      </div>
      <div className="flex gap-1">
        <SocialLink
          link="https://www.linkedin.com/in/anujv99/"
          icon={FaLinkedinIn}
        />
        <SocialLink link="https://gitlab.com/anujv" icon={FaGitlab} />
        <SocialLink link="https://github.com/anujv99" icon={FaGithub} />
        <div className="w-px flex-1 bg-primary m-1" />
        <ThemeSwitcher />
      </div>
    </NavbarLayout>
  );
};

export default Navbar;
