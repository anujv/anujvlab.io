"use client";

import React, { useCallback } from "react";
import { PiMoon, PiSun } from "react-icons/pi";

import { IconButton } from "../buttons";
import { cx } from "../../utils/helper";

type Props = {
  className?: string;
};

function getTheme(): "dark" | "light" {
  const theme = document.body.getAttribute("data-theme");
  if (theme === "dark") return "dark";
  return "light";
}

const ThemeSwitcher: React.FC<Props> = (props) => {
  const { className } = props;

  const toggleTheme = useCallback(() => {
    const newTheme = getTheme() === "dark" ? "light" : "dark";
    document.body.setAttribute("data-theme", newTheme);
  }, []);

  return (
    <>
      <IconButton
        className={cx("hidden dark:block", className)}
        icon={PiMoon}
        onClick={toggleTheme}
      />
      <IconButton
        className={cx("dark:hidden", className)}
        icon={PiSun}
        onClick={toggleTheme}
      />
    </>
  );
};

export default ThemeSwitcher;
