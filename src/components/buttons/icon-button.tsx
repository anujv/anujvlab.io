import React from "react";
import { IconType } from "react-icons";

import { cx } from "../../utils/helper";

type Props = {
  icon: IconType;
  children?: React.ReactNode;
  as?: "div" | "button";
  className?: string;
  onClick?: (e: React.MouseEvent) => void;
};

const IconButton: React.FC<Props> = (props) => {
  const { icon: Icon, children, as = "button", className, ...rest } = props;

  const Comp = as;

  return (
    <Comp className={cx("p-2 bg-transparent rounded", className)} {...rest}>
      <Icon className="w-4 h-4 text-primary opacity-70 hover:opacity-100 transition-opacity duration-200" />
      {children}
    </Comp>
  );
};

export default IconButton;
