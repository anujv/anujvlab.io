import React from "react";
import { cx } from "../../utils/helper";
import { PiEnvelopeSimple } from "react-icons/pi";

type Props = {
  className?: string;
};

const SayHi: React.FC<Props> = (props) => {
  const { className } = props;

  return (
    <div
      className={cx(
        "items-center justify-between hover-bg rounded-xl",
        "flex flex-col-reverse md:flex-row p-6 md:p-8 gap-2 md:gap-6",
        className,
      )}
    >
      <div className="flex flex-col items-start">
        <p className="text-4xl font-bold gradient-sunset-2 text-transparent bg-clip-text">
          Say Hello!
        </p>
        <p className="text-xl text-primary font-medium mt-3 md:mt-5">
          Please don't hesitate to reach out to me if you have any questions or
          just want to say hi.
        </p>
        <p className="text-lg font-medium mt-6">
          <PiEnvelopeSimple className="w-5 h-5 inline-block mr-1 text-[#1584EB]" />
          <a
            href="mailto:anujv@posteo.net"
            className="hover:underline gradient-sky-3 text-transparent bg-clip-text"
          >
            anujv@posteo.net
          </a>
        </p>
      </div>
      <img src="/img/3d-chat.png" alt="3D chat" width={275} height={275} />
    </div>
  );
};

export default SayHi;
