"use client";

import React, { useEffect, useRef, useState } from "react";
import init, { WgpuApp, getLimits } from "wgpu-shadertoy/wgpu_shadertoy";
import { useElementSize } from "../../utils/hooks";

const setWasmInitialized = (initialized: boolean) => {
  // @ts-ignore
  window.wasm_initialized = initialized;
};

const isWasmInitialized = () => {
  // @ts-ignore
  return window.wasm_initialized;
};

const setWasmInitializedCallback = (callback: () => void) => {
  // @ts-ignore
  window.wasm_initialized_callback = callback;
};

const wasmCall = (fn: () => void) => {
  if (isWasmInitialized()) fn();
};

if (typeof window !== "undefined") {
  window.addEventListener("load", () => {
    init()
      .then(() => {
        setWasmInitialized(true);
        // @ts-ignore
        window.wasm_initialized_callback?.();
        console.log("WASM initialized");
      })
      .catch(console.error);
  });
}

const Page: React.FC = () => {
  const { setEl, width, height } = useElementSize();

  const [initialized, setInitialized] = useState(false);
  const [maxSize, setMaxSize] = useState<number | undefined>(undefined);

  const wgpuAppRef = useRef<WgpuApp | null>(null);

  useEffect(() => {
    if (!isWasmInitialized()) {
      setWasmInitializedCallback(() => setInitialized(true));
    }
  }, []);

  useEffect(() => {
    if (!initialized) return;

    const wgpuApp = new WgpuApp({
      width: 1920,
      height: 1080,
    });

    const limits = getLimits();
    setMaxSize(limits.maxTextureDimension2D);
    wgpuAppRef.current = wgpuApp;
  }, [initialized]);

  return (
    <div
      id="wasm-canvas"
      className="w-full aspect-video"
      style={{
        maxWidth: maxSize,
        maxHeight: maxSize,
      }}
      ref={setEl}
    ></div>
  );
};

export default Page;
