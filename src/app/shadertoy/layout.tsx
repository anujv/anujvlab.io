import React from "react";

type ComponentProps = {
  children: React.ReactNode;
};

export const metadata = {
  title: "GG Shadertoy",
  description: "Shadetoy clone since actual shadertoy is down",
};

const RootLayout: React.FC<ComponentProps> = (props) => {
  const { children } = props;

  return <div>{children}</div>;
};

export default RootLayout;
