import React from "react";
import { Inter as CustomFont } from "next/font/google";

import "./globals.css";

const customFont = CustomFont({
  weight: ["400", "500", "600", "700"],
  subsets: ["latin"],
  display: "swap",
});

type Props = {
  children: React.ReactNode;
};

const RootLayout: React.FC<Props> = (props) => {
  const { children } = props;

  return (
    <html lang="en" className={customFont.className}>
      <body data-theme="dark">{children}</body>
    </html>
  );
};

export default RootLayout;
