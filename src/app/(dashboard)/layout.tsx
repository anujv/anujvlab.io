import React from "react";
import { Navbar } from "../../components/navbar";

type ComponentProps = {
  children: React.ReactNode;
};

export const metadata = {
  title: "Anuj Verma | Portfolio",
  description: "This is my next.js website",
};

const RootLayout: React.FC<ComponentProps> = (props) => {
  const { children } = props;

  return (
    <>
      <div className="gradient-bg" />
      <div className="p-4 w-full">
        <div className="max-w-3xl mx-auto">
          <Navbar />
          {children}
        </div>
      </div>
    </>
  );
};

export default RootLayout;
