import React from "react";
import { PiArrowUpRightBold } from "react-icons/pi";
import { SayHi } from "../../components/cards";

type ComponentProps = {};

const Page: React.FC<ComponentProps> = () => {
  return (
    <>
      <div className="flex flex-col items-start w-full px-2 md:px-4 py-4 mt-9">
        <div className="flex items-center gap-4">
          <img
            src="/img/dummy-avatar.jpeg"
            width={90}
            height={90}
            alt="Avatar of author"
            className="rounded-full"
          />
          <div className="flex flex-col">
            <h1 className="text-2xl font-bold text-primary font-semibold">
              Anuj Verma
            </h1>
            <p className="text-sm text-light-gray font-medium flex items-center">
              @anujv99 <PiArrowUpRightBold className="w-4 h-4" />
            </p>
          </div>
        </div>
        <p className="text-base md:text-lg text-primary my-4">
          Hi, I'm Anuj Verma, a frontend and graphics developer from India.
          Outside of work, I spend most of my time learning new thing and
          building side projects. I first became intrested in graphics
          programming in 2017 when I started learning OpenGL, and now I'm
          learning Vulkan.
        </p>
        <p className="text-base md:text-lg text-light-gray font-medium">
          Frontend Developer at&nbsp;
          <a
            className="gradient-retro-2 bg-clip-text text-transparent"
            href="https://www.womp.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            Womp 3D
          </a>
        </p>
        <SayHi className="mt-4 md:mt-12 w-full" />
      </div>
    </>
  );
};

export default Page;
