
use std::{borrow::BorrowMut, sync::{Arc, Mutex}};

#[path = "./types.rs"] mod types;

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;
#[cfg(target_arch = "wasm32")]
use js_sys;
use winit::{dpi::PhysicalSize, event::{ElementState, Event, KeyEvent, WindowEvent}, event_loop::{EventLoop, EventLoopWindowTarget}, keyboard::{KeyCode, PhysicalKey}, window::{Window, WindowBuilder}};

#[derive(Default)]
pub struct WinitState {
  window: Option<Arc<Window>>,
  size: PhysicalSize<u32>,

  #[cfg(target_arch = "wasm32")]
  create_window_callback: Option<js_sys::Function>,
  #[cfg(target_arch = "wasm32")]
  resize_window_callback: Option<js_sys::Function>,
}

#[cfg_attr(target_arch = "wasm32", wasm_bindgen)]
pub struct WgpuApp {
  window_width: u32,
  window_height: u32,
  gfx_state: Arc<Mutex<GfxState>>,
}

pub struct GfxState {
  device: wgpu::Device,
  queue: wgpu::Queue,
  config: wgpu::SurfaceConfiguration,
  size: winit::dpi::PhysicalSize<u32>,
  surface: wgpu::Surface<'static>,
  window: Arc<Window>,
  limits: wgpu::Limits,
}

#[derive(Default)]
pub struct WgpuParams {
  pub width: u32,
  pub height: u32,
}

impl WgpuApp {
  pub async fn new(params: &WgpuParams) -> Self {
    cfg_if::cfg_if! {
      if #[cfg(target_arch = "wasm32")] {
        std::panic::set_hook(Box::new(console_error_panic_hook::hook));
        console_log::init_with_level(log::Level::Debug).expect("couldn't initialize logger");
      } else {
        env_logger::init();
      }
    }

    let width: u32 = params.width;
    let height: u32 = params.height;

    log::info!("[WgpuApp::new] ({}, {})", width, height);

    let event_loop = EventLoop::new().unwrap();
    let window = Arc::new(WindowBuilder::new().build(&event_loop).unwrap());

    #[cfg(target_arch = "wasm32")] {
      use winit::platform::web::WindowExtWebSys;
      web_sys::window()
        .and_then(|win| win.document())
        .and_then(|doc| {
          let dst = doc.get_element_by_id("wasm-canvas")?;
          let canvas = web_sys::Element::from(window.clone().canvas()?);
          canvas.set_attribute("width", "100%").ok()?;
          canvas.set_attribute("height", "100%").ok()?;
          canvas.set_attribute("style", "width: 100%; height: 100%;").ok()?;
          dst.append_child(&canvas).ok()?;
          Some(())
        })
      .expect("couldn't append canvas to element");

      // let _ = window.request_inner_size(PhysicalSize::new(width, height));
    }

    let state = Arc::new(Mutex::new(GfxState::new(window).await));
    let state_clone = state.clone();

    let mut surface_configured = false;

    let event_handler_fn = move |event: Event<()>, control_flow: &EventLoopWindowTarget<()>| {
      let current_window_id = state.lock().unwrap().get_window().id();

      match event {
        Event::Resumed => {
          log::debug!("Resumed");
        }
        Event::WindowEvent {
          ref event,
          window_id,
        } if window_id == current_window_id => match event {
          WindowEvent::CloseRequested
            | WindowEvent::KeyboardInput {
              event:
                KeyEvent {
                  state: ElementState::Pressed,
                  physical_key: PhysicalKey::Code(KeyCode::Escape),
                  ..
                },
                ..
            } => control_flow.exit(),
          WindowEvent::Resized(physical_size) => {
            log::info!("Resized: {:?}", physical_size);
            state.lock().unwrap().resize(*physical_size);
            surface_configured = true;
          },
          WindowEvent::RedrawRequested => {
            let mut mut_state = state.lock().unwrap();
            mut_state.get_window().request_redraw();

            if !surface_configured {
              return;
            }

            match mut_state.render() {
              Ok(_) => {},
              Err(..) => {
                log::error!("Error rendering");
              }
            }
          },
          _ => {},
        },
        _ => {}
      }
    };

    #[cfg(not(target_arch = "wasm32"))] {
      event_loop.run(event_handler_fn).unwrap()
    }
    #[cfg(target_arch = "wasm32")] {
      use winit::platform::web::EventLoopExtWebSys;
      event_loop.spawn(event_handler_fn);
    }

    WgpuApp {
      window_width: width,
      window_height: height,
      gfx_state: state_clone,
    }
  }
}

impl GfxState {
  pub async fn new(window: Arc<Window>) -> Self {
    let size = window.inner_size();

    let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
      #[cfg(not(target_arch = "wasm32"))]
        backends: wgpu::Backends::PRIMARY,
      #[cfg(target_arch = "wasm32")]
        backends: wgpu::Backends::GL,
      ..Default::default()
    });

    let surface = instance.create_surface(window.clone()).unwrap();

    log::info!("window size: {:?}", size);

    let adapter = instance.request_adapter(&wgpu::RequestAdapterOptions {
      power_preference: wgpu::PowerPreference::default(),
      compatible_surface: Some(&surface),
      force_fallback_adapter: false,
    }).await.unwrap();

    let (device, queue) = adapter.request_device(&wgpu::DeviceDescriptor {
      label: None,
      required_features: wgpu::Features::empty(),
      #[cfg(not(target_arch = "wasm32"))]
        required_limits: wgpu::Limits::default(),
      #[cfg(target_arch = "wasm32")]
        required_limits: wgpu::Limits::downlevel_webgl2_defaults(),
      memory_hints: Default::default(),
    }, None).await.unwrap();

    let surface_caps = surface.get_capabilities(&adapter);

    let surface_format = surface_caps.formats
      .iter()
      .copied()
      .find(|f| f.is_srgb())
      .unwrap_or(surface_caps.formats[0]);

    let config = wgpu::SurfaceConfiguration {
      usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
      format: surface_format,
      width: size.width,
      height: size.height,
      present_mode: surface_caps.present_modes[0],
      alpha_mode: surface_caps.alpha_modes[0],
      desired_maximum_frame_latency: 2,
      view_formats: vec![],
    };

    log::info!("[GfxState::new] state created");
    
    GfxState {
      device,
      queue,
      config,
      size,
      surface,
      window: window.clone(),
      limits: wgpu::Limits::downlevel_webgl2_defaults(),
    }
  }

  pub fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
    let output = self.surface.get_current_texture()?;
    let view = output.texture.create_view(&wgpu::TextureViewDescriptor::default());
    let mut encoder = self.device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
      label: Some("render encoder"),
    });

    {
      let _render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
        label: Some("render pass"),
        color_attachments: &[Some(wgpu::RenderPassColorAttachment {
          view: &view,
          resolve_target: None,
          ops: wgpu::Operations {
            load: wgpu::LoadOp::Clear(wgpu::Color::RED),
            store: wgpu::StoreOp::Store,
          },
        })],
        depth_stencil_attachment: None,
        occlusion_query_set: None,
        timestamp_writes: None,
      });
    }

    self.queue.submit(std::iter::once(encoder.finish()));
    output.present();

    Ok(())
  }

  pub fn resize(&mut self, new_size: PhysicalSize<u32>) {
    if new_size.width > 0 && new_size.height > 0 {
      self.size = new_size;
      self.config.width = std::cmp::min(new_size.width, self.limits.max_texture_dimension_2d);
      self.config.height = std::cmp::min(new_size.height, self.limits.max_texture_dimension_2d);
      self.surface.configure(&self.device, &self.config);
    }
  }

  pub fn get_window(&self) -> Arc<Window> {
    self.window.clone()
  }
}

#[cfg_attr(target_arch = "wasm32", wasm_bindgen)]
impl WgpuApp {
  #[cfg(target_arch = "wasm32")]
  #[wasm_bindgen(constructor)]
  pub async fn new_js(ts_params: types::IWgpuParams) -> Self {
    let params: JsValue = ts_params.into();
    use js_sys::wasm_bindgen::JsValue;

    let mut width: u32 = 1280;
    let mut height: u32 = 720;

    if params.is_object() {
      if js_sys::Reflect::has(&params, &JsValue::from_str("width")).unwrap() {
        width = js_sys::Reflect::get(&params, &JsValue::from_str("width")).unwrap().as_f64().unwrap() as u32;
      }
      if js_sys::Reflect::has(&params, &JsValue::from_str("height")).unwrap() {
        height = js_sys::Reflect::get(&params, &JsValue::from_str("height")).unwrap().as_f64().unwrap() as u32;
      }
    }

    WgpuApp::new(&WgpuParams {
      width,
      height,
    }).await
  }

  #[cfg_attr(target_arch = "wasm32", wasm_bindgen(js_name = getWidth))]
  pub fn get_width(&self) -> u32 {
    self.window_width
  }

  #[cfg_attr(target_arch = "wasm32", wasm_bindgen(js_name = getHeight))]
  pub fn get_height(&self) -> u32 {
    self.window_height
  }
}

