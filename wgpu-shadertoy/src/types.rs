
#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen(typescript_custom_section)]
const IWgpuParams: &'static str = r#"
interface IWgpuParams {
  width: number;
  height: number;
}
"#;

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
extern "C" {
  #[wasm_bindgen(typescript_type = "IWgpuParams")]
  pub type IWgpuParams;
}
