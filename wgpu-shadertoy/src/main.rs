
mod app;

fn main() {
  let app = app::WgpuApp::new(&app::WgpuParams {
    width: 1280,
    height: 720,
  });
  pollster::block_on(app);
}
