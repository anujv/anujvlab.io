
#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
pub struct WgpuLimits {
  #[wasm_bindgen(js_name = maxTextureDimension2D)]
  pub max_texture_dimension_2d: u32,
}

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen(js_name = getLimits)]
pub fn get_limits() -> WgpuLimits {
  let limits = wgpu::Limits::downlevel_webgl2_defaults();
  WgpuLimits {
    max_texture_dimension_2d: limits.max_texture_dimension_2d,
  }
}
