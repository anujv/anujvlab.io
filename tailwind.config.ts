import type { Config } from "tailwindcss";
import { createThemes } from "tw-colors";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  darkMode: ["class", '[data-theme="dark"]'],
  theme: {
    extend: {
      colors: {
        black: "#0A0A0D",
        white: "#FFFFFF",
        yellow: "#F8C231",
        "light-yellow": "#F8C231",
        red: "#FF445E",
        "light-red": "#FFC5CF",
        pink: "#FF5AD1E5",
        "light-pink": "#FFE9FA",
        green: "#00BB83CC",
        "light-green": "#BEFFEC",
        blue: "#0560FFCC",
        "light-blue": "#B7F2FF",
        orange: "#FF6928CC",
        "light-orange": "#FFE5D7",
        purple: "#4D3EFFE5",
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
    },
  },
  plugins: [
    createThemes({
      light: {
        primary: "#22242C",
        "light-gray": "#4A576FB2",
        gray: {
          "100": "#30344D33",
        },
        main: "#FFFFFF",
        nav: "#FFFFFF4D",
        "nav-menu": "#0A0A0D",
      },
      dark: {
        primary: "#ECEDEE",
        "light-gray": "#727888F5",
        gray: {
          "100": "#FFFFFF33",
        },
        main: "#0A0A0D",
        nav: "#1C162F4D",
        "nav-menu": "#1C162F33",
      },
    }),
  ],
};
export default config;
